<?php 
session_start();
$user=isset($_SESSION['user_id'])?$_SESSION['user_id']:"";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" ng-app="carRentingApp">
  <!--head -->
  <?php include('partials/home/head.php');?>
  <!-- /head -->
  <body ng-controller="homeCtrl" style="background-color:#D7DBDD;">
    <div class="container">
      <!--head -->
      <?php include('partials/home/navbar.php');?>
      <!-- /head -->
      <!-- start slideshow -->
      <div class="row" >
	    	<div class="col-sm-1"></div>
	      <div class="col-sm-10">
	    		<br>
	      	<div id="car_slide" class="carousel slide" data-ride="carousel">
		        <!-- Indicators -->
		        <ul class="carousel-indicators">
		          <li data-target="#car_slide" data-slide-to="0" class="active"></li>
		          <li data-target="#car_slide" data-slide-to="1"></li>
		          <li data-target="#car_slide" data-slide-to="2"></li>
		      		<li data-target="#car_slide" data-slide-to="3"></li>
		        </ul>
		        <!-- The slideshow -->
		        <div class="carousel-inner">
		        	<!-- Car sliding -->
		          <div class="carousel-item active">
		            <img src="dist/images/featured-img-1.jpg" alt="Trees" width="1000" height="500">
		          </div>
		        </div>
		        <!-- Left and right controls -->
		        <a class="carousel-control-prev" href="#car_slide" data-slide="prev">
		          <span class="carousel-control-prev-icon"></span>
		        </a>
		        <a class="carousel-control-next" href="#car_slide" data-slide="next">
		          <span class="carousel-control-next-icon"></span>
		        </a>
		      </div>
	      </div>
	      <div class="col-sm-1" ></div>
      </div>
	    <!--end slideshow -->
	    <!--start content-->	
	    <div class="row">
	    	<input type="hidden" id="user_id" value="<?php echo $user; ?>">
		    <div class="col-sm-4" style="margin-top:10px;" ng-repeat="car in cars">
		     	<div class="card" >
		        <img class="card-img-top" src="images/car_photos/{{car.images[0].image}}" alt="Card image" style="width:100%">
		        <div class="card-body">
		        	<span class="pull-right">
		        		Booked:<b>{{car.is_booked|uppercase}}</b>
		        	</span>
		          <h6 class="card-title" ng-bind="car.name|uppercase"></h6>
		          <p class="card-text" ng-bind="car.description"></p>
		    			<b ng-bind="car.price+'Rwf'" class="pull-right text-success"></b>
		    			<!-- <button class="btn btn-sm btn-success" ng-click="addToCard(car)">Add to card</button>
		    			<button class="btn btn-sm btn-danger" ng-click="rmvToCard(car)">Remove to card</button> -->
		    			<button class="btn btn-info btn-sm" ng-click="openModal(car)">Book now</button>
		        </div>
		      </div>
		    </div>
		  </div>
			<!--end content -->
		  <!--footer-->
		  <?php 
		  include('partials/home/footer.php');
		  include 'partials/modals.php';
		  ?>
		  <!-- /footer -->
	  </div>
  </body>
</html>