<?php
/**
 * @author Akimana JA
 * @AJAkimana  */
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
// json response array
$response = array("error" => FALSE);
$_POST = (array)json_decode(file_get_contents('php://input'), true);
if ($_POST['name'] && $_POST['email'] && $_POST['password']) {
  // receiving the post params
  $name = $_POST['name'];
  $email = $_POST['email'];
  $password = $_POST['password'];
  $location = $_POST['location'];
  $username = $_POST['username'];
  $access=4;
  // check if user is already existed with the same email
  if ($db->isUserExisted($email)) {
    // user already existed
    $response["error"] = TRUE;
    $response["error_msg"] = "User already existed with " . $email;
    echo json_encode($response);
  }else{
    // create a new user
    $user = $db->storeUser($name, $username, $email, $location, $access, $password);
    if ($user) {
      // user stored successfully
      $response["error"] = FALSE;
      // $response["uid"] = $user["unique_id"];
      $response["user"]["name"] = $user["name"];
      $response["user"]["email"] = $user["email"];
      $response["user"]["created_at"] = $user["created_at"];
      $response["user"]["updated_at"] = $user["updated_at"];
      echo json_encode($response);
    }else{
      // user failed to store
      $response["error"] = TRUE;
      $response["error_msg"] = "Unknown error occurred in registration!";
      echo json_encode($response);
    }
  }
}else{
  $response["error"] = TRUE;
  $response["error_msg"] = "Required parameters (name, email or password) is missing!";
  echo json_encode($response);
}
?>

