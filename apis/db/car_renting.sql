-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 15, 2018 at 09:07 
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_renting`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `book_from` datetime NOT NULL,
  `book_to` datetime NOT NULL,
  `confirm` enum('no','yes') NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `user_id`, `car_id`, `book_from`, `book_to`, `confirm`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '2018-05-11 11:47:33', '2018-05-18 11:47:33', 'yes', '2018-05-10 13:47:45', '2018-05-10 13:58:43');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `color` enum('red','green','blue','white','brown') NOT NULL,
  `plaque` varchar(10) NOT NULL,
  `type` enum('little','big') NOT NULL,
  `is_booked` enum('no','yes') NOT NULL DEFAULT 'no',
  `price` float NOT NULL,
  `is_active` enum('no','yes') NOT NULL DEFAULT 'yes',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `name`, `description`, `color`, `plaque`, `type`, `is_booked`, `price`, `is_active`, `created_at`, `updated_at`, `company_id`) VALUES
(1, 'Lamborgin', 'The best and the fastest car you ever get. You get in you drive you get the with no time', 'red', 'RAC 580H', 'little', 'yes', 1234000, 'yes', '2018-05-10 13:42:18', '2018-05-10 13:42:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_photos`
--

CREATE TABLE `car_photos` (
  `id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_photos`
--

INSERT INTO `car_photos` (`id`, `car_id`, `image`) VALUES
(1, 1, '5af4302f54bba5.07226713.png');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `owner` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `owner`, `location`, `created_at`, `updated_at`) VALUES
(1, 'Company Rent Ltd', 2, 'Kigali', '2018-05-10 13:22:10', '2018-05-10 13:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `company_members`
--

CREATE TABLE `company_members` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `user_id_from` int(11) NOT NULL,
  `user_id_dest` int(11) NOT NULL,
  `seen` enum('no','yes') NOT NULL,
  `is_for_company` enum('no','yes') NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `content`, `user_id_from`, `user_id_dest`, `seen`, `is_for_company`, `created_at`, `updated_at`) VALUES
(1, 'Irankunda Benon has requested the car. NAME:Lamborgin from:2018/05/11 11:47:33 to:2018/05/18 11:47:33', 3, 1, 'no', 'yes', '2018-05-10 13:47:45', '2018-05-10 13:47:45'),
(2, 'Akimana Ajouly Jean dAmour has responded to your request on car: Lamborgin', 1, 3, 'no', 'no', '2018-05-10 13:58:43', '2018-05-10 13:58:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `access_lv` enum('1','2','3','4') NOT NULL,
  `location` varchar(50) NOT NULL,
  `is_active` enum('no','yes') NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `salt`, `email`, `access_lv`, `location`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Akimana Ajouly Jean dAmour', 'Akimana', '0KsKq43HH4rHpWukY6MfOzAzKJNkODk4YmFiYzA2', 'd898babc06', 'akimana@email.com', '1', 'Kigali', 'no', '2018-05-10 13:14:59', '2018-05-10 13:14:59'),
(2, 'Kanimba Emanuel', 'Kanimba', 'ZKR1wjEP9UjIQVMHhBlkuaASXPo4NGVkMzgyOWFk', '84ed3829ad', 'kanimba@email.com', '2', 'Kigali', 'yes', '2018-05-10 13:20:44', '2018-05-10 13:22:10'),
(3, 'Irankunda Benon', 'IranBe', 'TYBRotJ8oEfHkDpCR1OH8RTOJLc0ZjI3NTYwZTY0', '4f27560e64', 'irankunda@email.com', '4', 'Kicukiro', 'yes', '2018-05-10 13:46:57', '2018-05-10 13:46:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_booking_fk` (`user_id`),
  ADD KEY `car_booking_fk` (`car_id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_car_fk` (`company_id`);

--
-- Indexes for table `car_photos`
--
ALTER TABLE `car_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `car_car_photo_fk` (`car_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_company_fk` (`owner`);

--
-- Indexes for table `company_members`
--
ALTER TABLE `company_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `company_members_ibfk_1` (`company_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `car_photos`
--
ALTER TABLE `car_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `company_members`
--
ALTER TABLE `company_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `car_booking_fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`);

--
-- Constraints for table `cars`
--
ALTER TABLE `cars`
  ADD CONSTRAINT `company_car_fk` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `car_photos`
--
ALTER TABLE `car_photos`
  ADD CONSTRAINT `car_car_photo_fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `user_company_fk` FOREIGN KEY (`owner`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
