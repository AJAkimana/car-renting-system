-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 09, 2018 at 11:30 
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_renting`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `book_from` datetime NOT NULL,
  `book_to` datetime NOT NULL,
  `confirm` enum('no','yes') NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `user_id`, `car_id`, `book_from`, `book_to`, `confirm`, `created_at`, `updated_at`) VALUES
(1, 6, 5, '2018-05-09 21:32:28', '2018-05-10 08:32:28', 'no', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `color` enum('red','green','blue','white','brown') NOT NULL,
  `plaque` varchar(10) NOT NULL,
  `type` enum('little','big') NOT NULL,
  `is_booked` enum('no','yes') NOT NULL DEFAULT 'no',
  `price` float NOT NULL,
  `is_active` enum('no','yes') NOT NULL DEFAULT 'yes',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `name`, `description`, `color`, `plaque`, `type`, `is_booked`, `price`, `is_active`, `created_at`, `updated_at`, `company_id`) VALUES
(3, 'Limousine', 'Best car booking, with fully equipped cars. fast service and affordable prices', 'red', 'RAB 234F', 'little', 'no', 30000, 'yes', '2018-05-02 17:21:11', '2018-05-07 13:40:21', 5),
(5, 'Hammer', 'You want to be where you want any time soon, just use this it will not hurt yoju', 'green', 'RAB 398Z', 'little', 'yes', 34000, 'yes', '2018-05-03 09:32:39', '2018-05-07 13:39:34', 5),
(6, 'Lambogin', 'Be the change you want to see in the world', 'red', 'RAA 436F', 'little', 'no', 50000, 'yes', '2018-05-07 13:27:43', '2018-05-07 13:27:43', 5);

-- --------------------------------------------------------

--
-- Table structure for table `car_photos`
--

CREATE TABLE `car_photos` (
  `id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_photos`
--

INSERT INTO `car_photos` (`id`, `car_id`, `image`) VALUES
(1, 3, '5af03302b813c1.93829013.jpg'),
(2, 5, '5af03317292a77.78079226.jpg'),
(3, 6, '5af0388842cd62.48735940.png');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `owner` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `owner`, `location`, `created_at`, `updated_at`) VALUES
(5, 'Sytem Rent Ltd', 12, 'Kgali/Gitega', '2018-05-02 12:13:41', '2018-05-03 18:41:14');

-- --------------------------------------------------------

--
-- Table structure for table `company_members`
--

CREATE TABLE `company_members` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `content` varchar(100) NOT NULL,
  `user_id_from` int(11) NOT NULL,
  `user_id_dest` int(11) NOT NULL,
  `seen` enum('no','yes') NOT NULL,
  `is_for_company` enum('no','yes') NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `content`, `user_id_from`, `user_id_dest`, `seen`, `is_for_company`, `created_at`, `updated_at`) VALUES
(1, 'First notification for test', 6, 7, 'no', 'no', '2018-04-29 11:40:16', '2018-04-29 11:40:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `access_lv` enum('1','2','3','4') NOT NULL,
  `location` varchar(50) NOT NULL,
  `is_active` enum('no','yes') NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `salt`, `email`, `access_lv`, `location`, `is_active`, `created_at`, `updated_at`) VALUES
(6, 'Akimana Jean dAmour', 'Akimana', 'UKJr3yB4OrQ0C14ukPB0LsIclO8zZWQ4NjNiMjZk', '3ed863b26d', 'akimana@email.com', '4', 'Kigali', 'no', '2018-04-29 09:51:24', '2018-04-29 09:51:24'),
(7, 'Ishimwe Akimana Ella', 'Ishimwe', 'fnT1oGQeaNHfXrMHu/5qbMHQU49kOTRjOTViZjFm', 'd94c95bf1f', 'ishimwe@gmail.com', '4', 'Gitega', 'no', '2018-04-29 11:46:41', '2018-04-29 11:46:41'),
(8, 'Kalisa Emmanuel', 'Kalisa', 'yI9mtxzE0kJGMQDmOLYBXz2/tvAwOGZlYTBhZGE2', '08fea0ada6', 'kalisa@email.com', '4', 'Kigali', 'yes', '2018-04-30 13:39:29', '2018-04-30 13:39:29'),
(9, 'Me Karekezi', 'Karek', 'd4UDMEJBzI6LI2lmq+t7WeZsFYphY2FhOTc1YzU1', 'acaa975c55', 'me@email.com', '4', 'location', 'yes', '2018-04-30 13:48:50', '2018-04-30 13:48:50'),
(11, 'User For Testin', 'User', 'T3hG1R9qPS+VQInEuMJLs6rALOUzZWM2ZTA4MzFi', '3ec6e0831b', 'user@email.com', '4', 'Kigali', 'yes', '2018-04-30 13:55:01', '2018-04-30 13:55:01'),
(12, 'Mbabazi Aline', 'Aline', 'I2nB9WsNIE1wxBcRFijQIMGLQWA1OGU3NGVlNzI4', '58e74ee728', 'mbabazi@email.com', '4', 'Kigali', 'yes', '2018-05-03 18:38:09', '2018-05-03 18:38:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `car_id` (`car_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `car_photos`
--
ALTER TABLE `car_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `car_id` (`car_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`);

--
-- Indexes for table `company_members`
--
ALTER TABLE `company_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `company_members_ibfk_1` (`company_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `car_photos`
--
ALTER TABLE `car_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `company_members`
--
ALTER TABLE `company_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
  ADD CONSTRAINT `bookings_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `cars`
--
ALTER TABLE `cars`
  ADD CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `car_photos`
--
ALTER TABLE `car_photos`
  ADD CONSTRAINT `car_photos_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `companies_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `users` (`id`);

--
-- Constraints for table `company_members`
--
ALTER TABLE `company_members`
  ADD CONSTRAINT `company_members_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
