<?php
/**
 * @author Akimana JA
 * @AJAkimana  */
require_once 'include/DB_Functions.php';
$db = new DB_Functions();

// json response array
$response = array("error" => FALSE);

$_POST = (array)json_decode(file_get_contents('php://input'), true);
if ($_POST['email_uname'] && $_POST['password']) {
    // receiving the post params
    $email = $_POST['email_uname'];
    $password = $_POST['password'];
    // get the user by email and password
    $user = $db->getUserByEmailAndPassword($email, $password);
    if ($user != false) {
        // use is found
        $response["error"] = FALSE;
        session_start();
        $_SESSION['user_id']=$user['id'];
        $_SESSION['user_names']=$user['name'];
        $_SESSION['user_name']=$user['username'];
        $_SESSION['user_email']=$user['email'];
        $_SESSION['user_access']=$user['access_lv'];
        $_SESSION['user_location']=$user['location'];
        $_SESSION['user_c_at']=$user['created_at'];
        $_SESSION['user_u_at']=$user['updated_at'];

        echo json_encode($response);
    } else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "Login credentials are wrong. Please try again!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters email or password is missing!";
    echo json_encode($response);
}
?>