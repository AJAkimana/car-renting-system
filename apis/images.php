<?php
$image='';
$path='';
if(isset($_POST['save_image'])||isset($_POST['save_profile'])){
	$file = $_FILES['file_img'];

	//File properties
	$file_name = $file['name'];
	$file_tmp = $file['tmp_name'];
	$file_size = $file['size'];
	$file_error = $file['error'];
	
	//work out the file extension
	$file_ext = explode('.',$file_name);
	$file_ext = strtolower(end($file_ext));

  $allowed = array('png','jpg','gif', 'jpeg');
	
	if(in_array($file_ext,$allowed)){
		if($file_error ===0){
			if($file_size <= 5242880){
				$new_file_name = uniqid('',true).'.'.$file_ext;
				if(isset($_POST['save_image'])){
					$path='../images/car_photos/'.$new_file_name;
					$image=$new_file_name;
				}
				if(isset($_POST['save_profile'])){
					$path='../images/user_profiles/'.$new_file_name;
					$image=$new_file_name;
				}
				if(move_uploaded_file($file_tmp, $path)){
					return $image;
				}else {
					$msg='Image(s) not moved';
	     		return $msg;	
				}
			}else{
				$msg='image(s) size is not supported';
				return $msg;
			}
		}else{
			$msg='There is a image error';
		  return $msg;
		}
	}else{
		$msg='Image(s) not allowed. Change image format and try again';
    return $msg;
	}
}
?>