<?php 
  session_start();
	require_once 'include/DB_Functions.php';
	$db = new DB_Functions();
  if(array_key_exists('card', $_SESSION)) $card=$_SESSION['card'];
  else $card=array();
  // json response array
	$response = array("error" => FALSE);
  if(isset($_GET['admin_dash'])){
  	$response["error"] = false;
  	$response["welcome"] = "Welcome to the php json";

  	$data = $db->dashboardContent();
  	$response["dashbord"] = $data;
  	echo json_encode($response);
  }
  else if(isset($_GET['books_dash'])){
    $response["error"] = false;
    $response["welcome"] = "Welcome to the php json";

    $data = $db->getDashBookings();
    $response["bookings"] = $data;
    echo json_encode($response);
  }
  else if(isset($_GET['users'])){
  	$response["error"] = false;
  	$response["welcome"] = "Welcome to the php json";

  	$users = $db->getAllUsers();
  	$response['users'] = $users;
  	echo json_encode($response);
  }
  else if(isset($_GET['user'])&&isset($_GET['user_id'])){
  	$id = $_GET['user_id'];
  	if($id){
	  	$response["error"] = false;
	  	$response["welcome"] = "Welcome to the php json";

	  	$user = $db->findUserById($id);
	  	$response['user'] = $user;
	  	echo json_encode($response);
	  }
	  else{
	  	$response["error"] = TRUE;
  		$response["error_msg"] = "Invalid data";
  		echo json_encode($response);
	  }
  }
  else if(isset($_GET['company'])){
  	$response["error"] = false;
  	$response["welcome"] = "Welcome to the php json";

  	$data = $db->getCompaniesJson();
  	$response['companies'] = $data;
  	echo json_encode($response);
  }
  else if(isset($_GET['add_company'])){
  	$name="";
  	$owner="";
  	$location="";
  	if(isset($_POST['name'])&&isset($_POST['name'])&&isset($_POST['name'])){
  		$name=$_POST['name'];
			$owner=$_POST['owner'];
			$location=$_POST['location'];
  	}else{
  		$_POST=(array)json_decode(file_get_contents('php://input'), true);
  		$name=$_POST['name'];
			$owner=$_POST['owner'];
			$location=$_POST['location'];
  	}
  	if($name!=""&&$owner!=""&&$location!=""){
  		if($db->isNameExists($name, null)){
  			$response["error"] = TRUE;
	  		$response["error_msg"] = "$name is taken";
	  		echo json_encode($response);
  		}else{
  			if($db->addCompany($name,$owner,$location)){
  				$db->changeUserAccess($owner,2);
  			}else{
          $response["error"] = TRUE;
          $response["error_msg"] = "Servive not available";
          echo json_encode($response);
        }
  		}
  	}else{
  		$response["error"] = TRUE;
  		$response["error_msg"] = "Sorry, there is an error. Invalid data";
  		echo json_encode($response);
  	}
  }
  else if(isset($_GET['edit_company'])){
  	$name="";
  	$owner="";
  	$location="";
  	$id="";
  	if(isset($_POST['id'])&&isset($_POST['location'])&&isset($_POST['name'])&&isset($_POST['owner'])){
  		$id=$_POST['id'];
  		$name=$_POST['name'];
			$owner=$_POST['owner'];
			$location=$_POST['location'];
  	}else{
  		$_POST=(array)json_decode(file_get_contents('php://input'), true);
  		$id=$_POST['id'];
  		$name=$_POST['name'];
			$owner=$_POST['owner'];
			$location=$_POST['location'];
  	}
  	if($name!=""&&$owner!=""&&$location!=""&&$id!=""){
  		if($db->isNameExists($name, $id)){
  			$response["error"] = TRUE;
	  		$response["error_msg"] = "$name is taken";
	  		echo json_encode($response);
  		}else{
  			if(!$db->editCompany($name,$owner,$location,$id)){
  				$response["error"] = TRUE;
		  		$response["error_msg"] = "Servive not available";
		  		echo json_encode($response);
  			}
  		}
  	}else{
  		$response["error"] = TRUE;
  		$response["error_msg"] = "Sorry, there is an error. Invalid data";
  		echo json_encode($response);
  	}
  }else if(isset($_GET['del_company'])){
  	$id="";
  	if(isset($_POST['id'])){
  		$id=$_POST['id'];
  	}else{
  		$_POST=(array)json_decode(file_get_contents('php://input'), true);
  		$id=$_POST['id'];
  	}
  	if($id!=""){
			if(!$db->delCompany($id)){
				$response["error"] = TRUE;
	  		$response["error_msg"] = "Servive not available";
	  		echo json_encode($response);
			}
  	}else{
  		$response["error"] = TRUE;
  		$response["error_msg"] = "Sorry, there is an error. Invalid data";
  		echo json_encode($response);
  	}
  }else if(isset($_GET['add_car'])){
  	$name="";$company="";$type="";$color="";$plate="";$price="";$desc="";
  	if(isset($_POST['name'])){
  		$name=$_POST['name'];
			if($_SESSION['user_access']==1) $company=$_POST['company'];
			$type=$_POST['type'];
			$color=$_POST['color'];
			$plate=$_POST['plate'];
			$price=$_POST['price'];
			$desc=$_POST['desc'];
  	}else{
  		$_POST=(array)json_decode(file_get_contents('php://input'), true);
  		$name=$_POST['name'];
			if($_SESSION['user_access']==1) $company=$_POST['company'];
			$type=$_POST['type'];
			$color=$_POST['color'];
			$plate=$_POST['plate'];
			$price=$_POST['price'];
			$desc=$_POST['desc'];
  	}
    if($_SESSION['user_access']==1) $company=$company;
    else{
      $company_details=$db->findCompanyByOwner($_SESSION['user_id']);
      $company=$company_details['id'];
    }
  	if($name!=""&&$company!=""&&$type!=""&&$color!=""&&$plate!=""&&$price!=""&&$desc!=""){
  		if(!$db->isCarExists($plate, $id)){
  			if(!$db->addCar($name,$color,$plate,$type,$price,$company,$desc)){
					$response["error"] = TRUE;
		  		$response["error_msg"] = "Servive not available";
		  		echo json_encode($response);
				}
  		}else{
  			$response["error"] = TRUE;
	  		$response["error_msg"] = "The plate $plate is registered to another car";
	  		echo json_encode($response);
  		}
  	}else{
  		$response["error"] = TRUE;
  		$response["error_msg"] = "Sorry, there is an error. Invalid data $name, $company, $type, $color, $plate, $price";
  		echo json_encode($response);
  	}
  }else if(isset($_GET['edit_car'])){
  	$id="";$name="";$company="";$type="";$color="";$plate="";$price="";$desc="";
  	if(isset($_POST['name'])){
  		$id=$_POST['id'];
  		$name=$_POST['name'];
			$company=$_POST['company_id'];
			$type=$_POST['type'];
			$color=$_POST['color'];
			$plate=$_POST['plaque'];
			$price=$_POST['price'];
			$desc=$_POST['description'];
  	}else{
  		$_POST=(array)json_decode(file_get_contents('php://input'), true);
  		$id=$_POST['id'];
  		$name=$_POST['name'];
			$company=$_POST['company_id'];
			$type=$_POST['type'];
			$color=$_POST['color'];
			$plate=$_POST['plaque'];
			$price=$_POST['price'];
			$desc=$_POST['description'];
  	}
  	if($id!=""&&$name!=""&&$company!=""&&$type!=""&&$color!=""&&$plate!=""&&$price!=""&&$desc!=""){
  		if(!$db->isCarExists($plate, $id)){
  			if(!$db->editCar($id,$name,$company,$type,$color,$plate,$price,$desc)){
					$response["error"] = TRUE;
		  		$response["error_msg"] = "Servive not available";
		  		echo json_encode($response);
				}
  		}else{
  			$response["error"] = TRUE;
	  		$response["error_msg"] = "The plate $plate is registered to another car";
	  		echo json_encode($response);
  		}
  	}else{
  		$response["error"] = TRUE;
  		$response["error_msg"] = "Sorry, there is an error. Invalid data $name, $company, $type, $color, $plate, $price";
  		echo json_encode($response);
  	}
  }else if(isset($_GET['del_car'])){
  	$id="";
  	if(isset($_POST['id'])){
  		$id=$_POST['id'];
  	}else{
  		$_POST=(array)json_decode(file_get_contents('php://input'), true);
  		$id=$_POST['id'];
  	}
  	if($id!=""){
			if(!$db->delCar($id)){
				$response["error"] = TRUE;
	  		$response["error_msg"] = "Not deleted";
	  		echo json_encode($response);
			}
  	}else{
  		$response["error"] = TRUE;
  		$response["error_msg"] = "Sorry, there is an error. Invalid data";
  		echo json_encode($response);
  	}
  }else if(isset($_GET['set_active_car'])){
  	$id="";
  	if(isset($_POST['car_id'])){
  		$id=$_POST['car_id'];
  	}else{
  		$_POST=(array)json_decode(file_get_contents('php://input'), true);
  		$id=$_POST['car_id'];
  	}
  	if($id!=""){
			if(!$db->carChangeStatus($id)){
				$response["error"] = TRUE;
	  		$response["error_msg"] = "Not changed";
	  		echo json_encode($response);
			}
  	}else{
  		$response["error"] = TRUE;
  		$response["error_msg"] = "Sorry, there is an error. Invalid data";
  		echo json_encode($response);
  	}
  }else if(isset($_GET['car'])){
  	$response["error"] = false;
  	$response["welcome"] = "Welcome to the php json";

  	$data = $db->getCarsJson();
  	$response['cars'] = $data;
  	echo json_encode($response);
  }else if(isset($_GET['company_car'])){ //Get company cars
    if(isset($_SESSION['user_id'])) {
      $user=$_SESSION['user_id'];
      $response["error"] = false;
      $response["welcome"] = "Welcome to the php json";

      $data = $db->getCompanyCars($user);
      $company_details=$db->findCompanyByOwner($user);
      $response['company']=$company_details['name'];
      $response['cars'] = $data;
      echo json_encode($response);
    }else{
      $response["error"] = true;
      $response["welcome"] = "Welcome to the php json";
      $response['error_msg'] = "Unknown user";
      echo json_encode($response);
    }
  }else if(isset($_GET['cars_list'])){
  	$response["error"] = false;
  	$response["welcome"] = "Welcome to the php json";

  	$data = $db->findAllCarInfos();
  	$response['cars'] = $data;
  	echo json_encode($response);
  }else if(isset($_GET['images'])&&isset($_GET['car_id'])&&$_GET['car_id']){
    $car_id=$_GET['car_id'];
    $response["error"] = false;
    $response["welcome"] = "Welcome to the php json";

    $data = $db->findImagesByCar($car_id);
    $response['images'] = $data;
    echo json_encode($response);
  }else if(isset($_GET['bookings'])){
    $response["error"] = false;
    $response["welcome"] = "Welcome to the php json";

    $data = $db->getAllBookings();
    $response['bookings'] = $data;
    echo json_encode($response);
  }else if(isset($_GET['user_bookings'])){
    if(isset($_SESSION['user_id'])) {
      $user=$_SESSION['user_id'];
      $response["error"] = false;
      $response["welcome"] = "Welcome to the php json";

      $data = $db->getUserBookings($user);
      $response['bookings'] = $data;
      echo json_encode($response);
    }else{
      $response["error"] = true;
      $response["welcome"] = "Welcome to the php json";
      $response['error_msg'] = "Unknown user";
      echo json_encode($response);
    }
  }else if(isset($_GET['confirm_book'])){
    $user=$id="";
    if(isset($_POST['id'])){
      $id=$_POST['id'];
      $user=$_POST['user_id'];
    }else{
      $_POST=(array)json_decode(file_get_contents('php://input'), true);
      $id=$_POST['id'];
      $user=$_POST['user_id'];
    }
    if($id!=""&&$user!=""){
      if($db->bookChangeConfirm($id)){
        $thisBook=$db->findBookingById($id);
        $thisCar=$db->findCarById($thisBook['car_id']);
        $thisUser=$db->findUserById($user);
        $notif_desc=$thisUser['name']." has responded to your request on car: ".$thisCar['name'];
        $db->addNotification($user,$thisBook['user_id'],$notif_desc,false);
      }else{
        $response["error"] = TRUE;
        $response["error_msg"] = "Not changed";
        echo json_encode($response);
      }
    }else{
      $response["error"] = TRUE;
      $response["error_msg"] = "Sorry, there is an error. Invalid data";
      echo json_encode($response);
    }
  }else if(isset($_GET['user_notifs'])){
    if(isset($_SESSION['user_id'])){
      if($_SESSION['user_access']==4){
        $notifs=$db->getNotifs($_SESSION['user_id'], "user");
      }
      else if($_SESSION['user_access']==2){
        $notifs=$db->getNotifs($_SESSION['user_id'], "owner");
      }
      else if($_SESSION['user_access']==1){
        $notifs=$db->getNotifs($_SESSION['user_id'], "all");
      }

      $response["error"] = false;
      $response["welcome"] = "Welcome to the php json";
      $response["notifs"] = $notifs;
      echo json_encode($response);
    }else{
      $response["error"] = TRUE;
      $response["error_msg"] = "Sorry, Not logged in";
      echo json_encode($response);
    }
  }else if(isset($_GET['action'])&&$_GET['action']){
    $action=$_GET['action'];
    if($action=='add' && $_SERVER['REQUEST_METHOD']=='POST'){
      $id=$name=$price="";
      if(isset($_POST['id'])){
        $id=$_POST['id'];
        $name=$_POST['name'];
        $price=$_POST['price'];
      }else{
        $req=(array)json_decode(file_get_contents('php://input'), true);
        $id=$req['id'];
        $name=$req['name'];
        $price=$req['price'];
      }
      $req=(array)json_decode(file_get_contents('php://input'), true);
      $column = array_column($card, 'id');
      $found=array_search($id, $column);
      // $key=$db->checkKeyExist($_SESSION['card'], 'id', $id);
      if($found===false){
        $new_card=array('id'=>$id,'name'=>$name,'price'=>$price);
        $_SESSION['card'][]=$new_card;
      }else{
        $response["error"] = true;
        $response["welcome"] = "Welcome to the php json";
        $response["error_msg"] = "Sorry this car was added $found";
        echo json_encode($response);
      }
    }if($action=='remove' && $_SERVER['REQUEST_METHOD']=='POST'){

    }else if($action=='book' && $_SERVER['REQUEST_METHOD']=='POST'){
      $booker=$car=$from=$to=$company="";
      if(isset($_POST['id'])){
        $car=$_POST['id'];
        $booker=$_POST['booker'];
        $from=$_POST['checkin'];
        $to=$_POST['checkout'];
        $company=$_POST['company_id'];
      }else{
        $_POST=(array)json_decode(file_get_contents('php://input'), true);
        $car=$_POST['id'];
        $booker=$_POST['booker'];
        $from=$_POST['checkin'];
        $to=$_POST['checkout'];
        $company=$_POST['company_id'];
      }
      if($booker!=""&&$car!=""&&$from!=""&&$to!=""&&$company!=""){
        $today = date_create('now');
        $datetime1 = date_create($from);
        $datetime2 = date_create($to);
        $interval = $datetime1->diff($datetime2);
        $f_datetime=new DateTime($from);
        $t_datetime=new DateTime($to);
        if($datetime1 < $today){
          $response["error"] = true;
          $response["welcome"] = "Welcome to the php json";
          $response['error_msg'] = "Sorry check in date must be greater than today";
          echo json_encode($response);
        }
        else if($datetime1 > $datetime2){
          $response["error"] = true;
          $response["welcome"] = "Welcome to the php json";
          $response['error_msg'] = "Sorry check out date must be greater than check in";
          echo json_encode($response);
        }else{
          if($db->isCarBooked($car)){
            $response["error"] = true;
            $response["welcome"] = "Welcome to the php json";
            $response['error_msg'] = "Sorry this car is already booked";
            echo json_encode($response);
          }
          else{
            if($db->addBooked($car,$booker,$from,$to)){
              $thisCar=$db->findCarById($car);
              $thisUser=$db->findUserById($booker);
              $notif_desc=$thisUser['name']." has requested the car. NAME:".$thisCar['name']." from:".date_format($f_datetime,"Y/m/d H:i:s")." to:".date_format($t_datetime,"Y/m/d H:i:s");
              $db->addNotification($booker,$company,$notif_desc,true);
            }else{
              $response["error"] = true;
              $response["welcome"] = "Welcome to the php json";
              $response['error_msg'] = 'Something went wrong';
              echo json_encode($response);
            }
          }
        }
      }else{
        $response["error"] = true;
        $response["welcome"] = "Welcome to the php json";
        $response['error_msg'] = 'Fill the blank';
        echo json_encode($response);
      }
    }else if($action=='view_all'){
      $response["error"] = false;
      $response["welcome"] = "Welcome to the php json";
      $response['cards'] = $card;
      echo json_encode($response);
    }
  }else{
  	$response["error"] = TRUE;
  	$response["error_msg"] = "Sorry, there is an error. Unknown request";
  	echo json_encode($response);
  }
?>