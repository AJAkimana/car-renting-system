<?php
/**
 * @author Akimana JA
 * @AJAkimana  */
class DB_Functions {
  private $conn;
  // constructor
  function __construct() {
      require_once 'DB_Connect.php';
      // connecting to database
      $db = new Db_Connect();
      $this->conn = $db->connect();
  }
  // destructor
  function __destruct() {
      
  }
  /**
   * Storing new user
   * returns user details
   */
  public function storeUser($name, $username, $email, $location, $access, $password) {
      $active;
      if($access==4) $active="yes";
      else $active="no";
      $uuid = uniqid('', true);
      $hash = $this->hashSSHA($password);
      $encrypted_password = $hash["encrypted"]; // encrypted password
      $salt = $hash["salt"]; // salt
      $stmt = $this->conn->prepare("INSERT INTO users(name, username, password, salt, email, access_lv, location, is_active, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?, NOW(), NOW())");
      $stmt->bind_param("ssssssss",$name, $username, $encrypted_password, $salt, $email, $access, $location, $active);
      $result = $stmt->execute();
      $stmt->close();
      // check for successful store
      if ($result) {
          $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = ?");
          $stmt->bind_param("s", $email);
          $stmt->execute();
          $user = $stmt->get_result()->fetch_assoc();
          $stmt->close();

          return $user;
      } else {
          return false;
      }
  }
  /**
   * Get user by email and password
   */
  public function getUserByEmailAndPassword($email, $password) {
      $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = ? OR username = ?");
      $stmt->bind_param("ss", $email, $email);
      if ($stmt->execute()) {
          $user = $stmt->get_result()->fetch_assoc();
          $stmt->close();

          // verifying user password
          $salt = $user['salt'];
          $encrypted_password = $user['password'];
          $hash = $this->checkhashSSHA($salt, $password);
          // check for password equality
          if ($encrypted_password == $hash) {
              // user authentication details are correct
              return $user;
          }
      } else {
          return NULL;
      }
  }
  /**
   * Check user is existed or not
   */
  public function isUserExisted($email) {
    $stmt = $this->conn->prepare("SELECT email from users WHERE email = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        // user existed 
        $stmt->close();
        return true;
    } else {
        // user not existed
        $stmt->close();
        return false;
    }
  }
  /**
   * Encrypting password
   * @param password
   * returns salt and encrypted password
   */
  public function hashSSHA($password) {
      $salt = sha1(rand());
      $salt = substr($salt, 0, 10);
      $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
      $hash = array("salt" => $salt, "encrypted" => $encrypted);
      return $hash;
  }
  /**
   * Decrypting password
   * @param salt, password
   * returns hash string
   */
  public function checkhashSSHA($salt, $password) {
      $hash = base64_encode(sha1($password . $salt, true) . $salt);
      return $hash;
  }
  // app Functions
  public function getAllUsers(){
    $users_list = array();
    $stmtusers=$this->conn->query("SELECT id,name,username,email,access_lv,location,is_active, created_at, updated_at FROM users WHERE NOT access_lv=1");
    while ($row=$stmtusers->fetch_assoc()) {
      # code...
      $users_list[]=$row;
    }
    return $res['users']=$users_list;
  }
  public function findUserById($user_id){
    $stmtusers=$this->conn->query("SELECT id,name,username,email,access_lv,location,is_active, created_at, updated_at FROM users WHERE id=$user_id");
    $user = $stmtusers->fetch_assoc();
    return $user;
  }
  public function findCompanyByOwner($user){
    $stmt=$this->conn->query("SELECT * FROM companies WHERE owner=$user");
    $company = $stmt->fetch_assoc();
    return $company;
  }
  public function findCompanyById($id){
    $stmt=$this->conn->query("SELECT * FROM companies WHERE id=$id");
    $company = $stmt->fetch_assoc();
    return $company;
  }
  public function changeUserAccess($user, $level){
    $stmt = $this->conn->prepare("UPDATE users SET access_lv=?,updated_at=NOW() WHERE id=?");
    $stmt->bind_param("ss",$level, $user);
    if($stmt->execute()) {
      // Data deleted
      return true;
    } else {
      // Not deleted
      return false;
    }
  }
  public function findBookingById($book_id){
    $stmt=$this->conn->query("SELECT * FROM bookings WHERE id=$book_id");
    $booking = $stmt->fetch_assoc();
    return $booking;
  }
  public function dashboardContent(){
    $users_list = array();
    $bookings_list = array();
    $stmt = $this->conn->query("SELECT COUNT(id) as users_n FROM users WHERE NOT access_lv=1");
    $stmt1 = $this->conn->query("SELECT COUNT(id) as companies_n FROM companies");
    $stmt2 = $this->conn->query("SELECT COUNT(id) as cars_n FROM cars");
    $stmt3 = $this->conn->query("SELECT COUNT(id) as bkings_n FROM bookings");
    $stmtbkings = $this->conn->query("SELECT * FROM bookings");
    $stmtusers=$this->conn->query("SELECT id,name,username,email,access_lv,location,is_active, created_at, updated_at FROM users WHERE NOT access_lv=1");
    $result = $stmt->fetch_assoc();
    $result1 = $stmt1->fetch_assoc();
    $result2 = $stmt2->fetch_assoc();
    $result3 = $stmt3->fetch_assoc();

    $res['users']['number']=$result['users_n'];
    while ($row=$stmtusers->fetch_assoc()) {
      # code...
      $users_list[]=$row;
    }
    $res['users']['list']=$users_list;
    $res['companies']=$result1['companies_n'];
    $res['cars']=$result2['cars_n'];
    $res['bookings']['number']=$result3['bkings_n'];
    while ($row=$stmtbkings->fetch_assoc()) {
      # code...
      $bookings_list[]=$row;
    }
    $res['bookings']['list']=$bookings_list;
    return $res;
  }
  public function addCar($name,$color,$plate,$type,$price,$company,$desc){
    $stmt = $this->conn->prepare("INSERT INTO cars (name,color,plaque,type,price,company_id,description) VALUES (?,?,?,?,?,?,?)");
    $stmt->bind_param("sssssss", $name,$color,$plate,$type,$price,$company,$desc);
    if($stmt->execute()) {
      // Data inserted
      return true;
    } else {
      // Not inserted
      return false;
    }
  }
  public function editCar($id,$name,$company,$type,$color,$plate,$price,$desc){
    $stmt = $this->conn->prepare("UPDATE cars SET name=?,color=?,plaque=?,type=?,price=?,company_id=?,description=?,updated_at=NOW() WHERE id=?");
    $stmt->bind_param("ssssssss", $name,$color,$plate,$type,$price,$company,$desc,$id);
    if($stmt->execute()) {
      // Data EDITED
      return true;
    } else {
      // Not edited
      return false;
    }
  }
  public function delCar($id){
    $stmt = $this->conn->prepare("DELETE FROM cars WHERE id=?");
    $stmt->bind_param("s", $id);
    if($stmt->execute()) {
      // Data deleted
      return true;
    } else {
      // Not deleted
      return false;
    }
  }
  public function carChangeStatus($id){
    $car = $this->findCarById($id);
    $car_status;
    if($car==null) return false;
    else{
      if($car['is_active']=="no") $car_status="yes";
      else $car_status="no";
    }
    $stmt = $this->conn->prepare("UPDATE cars SET is_active=?,updated_at=NOW() WHERE id=?");
    $stmt->bind_param("ss",$car_status,$id);
    if($stmt->execute()) {
      // Data deleted
      return true;
    } else {
      // Not deleted
      return false;
    }
  }
  public function findAllCarInfos(){
    $cars = array();
    $stmt=$this->conn->query("SELECT cars.id, cars.name, cars.is_booked, cars.description, cars.color, cars.price, companies.id as company_id, companies.name AS company_name, users.name AS company_owner FROM cars INNER JOIN companies ON cars.company_id=companies.id INNER JOIN users ON companies.owner=users.id");
    while ($row=$stmt->fetch_assoc()) {
      # code...
      $id=$row['id'];
      $images_stmt=$this->conn->query("SELECT image FROM car_photos WHERE car_id=$id");
      $images=array();
      while ($row_img=$images_stmt->fetch_assoc()) {
        # code...
        $images[]=$row_img;
      }
      $cars[]=array('id'=>$row['id'],
                    'name'=>$row['name'],
                    'is_booked'=>$row['is_booked'],
                    'color'=>$row['color'],
                    'price'=>$row['price'],
                    'description'=>$row['description'],
                    'company_id'=>$row['company_id'],
                    'company_name'=>$row['company_name'],
                    'company_owner'=>$row['company_owner'],
                    'images'=>$images);
    }
    return $cars;
  }
  public function getCarsJson(){
    $cars = array();
    $stmt=$this->conn->query("SELECT * FROM cars");
    $i=0;
    while ($row=$stmt->fetch_assoc()) {
      # code...
      $i++;
      $id=$row['id'];
      $num_images_stmt=$this->conn->query("SELECT COUNT(id) as images FROM car_photos WHERE car_id=$id");
      if($num_images_stmt){
        $result = $num_images_stmt->fetch_assoc();
        $n_images=$result['images'];
      }else $n_images=0;
      $cars[]=array('id'=>$row['id'],
                    'name'=>$row['name'],
                    'color'=>$row['color'],
                    'plaque'=>$row['plaque'],
                    'type'=>$row['type'],
                    'is_booked'=>$row['is_booked'],
                    'price'=>$row['price'],
                    'company_id'=>$row['company_id'],
                    'is_active'=>$row['is_active'],
                    'created_at'=>$row['created_at'],
                    'updated_at'=>$row['updated_at'],
                    'images'=>$n_images);
    }
    return $cars;
  }
  public function getCompanyCars($user){
    $cars = array();
    // $thisUser=$this->findUserById($user);
    $userCompany=$this->findCompanyByOwner($user);
    $stmt=$this->conn->query("SELECT * FROM cars WHERE company_id=".$userCompany['id']);
    $i=0;
    while ($row=$stmt->fetch_assoc()) {
      # code...
      $i++;
      $id=$row['id'];
      $num_images_stmt=$this->conn->query("SELECT COUNT(id) as images FROM car_photos WHERE car_id=$id");
      if($num_images_stmt){
        $result = $num_images_stmt->fetch_assoc();
        $n_images=$result['images'];
      }else $n_images=0;
      $cars[]=array('id'=>$row['id'],
                    'name'=>$row['name'],
                    'color'=>$row['color'],
                    'plaque'=>$row['plaque'],
                    'type'=>$row['type'],
                    'is_booked'=>$row['is_booked'],
                    'price'=>$row['price'],
                    'company_id'=>$row['company_id'],
                    'is_active'=>$row['is_active'],
                    'created_at'=>$row['created_at'],
                    'updated_at'=>$row['updated_at'],
                    'images'=>$n_images);
    }
    return $cars;
  }
  public function findCarById($car_id){
    $stmt=$this->conn->query("SELECT * FROM cars WHERE id=$car_id");
    $car = $stmt->fetch_assoc();
    return $car;
  }
  public function findImagesByCar($car_id){
    $images=array();
    $stmt=$this->conn->query("SELECT * FROM car_photos WHERE car_id=$car_id");
    while($row=$stmt->fetch_assoc()){
      $images[]=$row;
    }
    return $images;
  }
  public function addCompany($name, $owner, $loc){
    $stmt = $this->conn->prepare("INSERT INTO companies (name,owner,location) VALUES (?,?,?)");
    $stmt->bind_param("sss", $name,$owner,$loc);
    if($stmt->execute()) {
      // Data inserted
      return true;
    } else {
      // Not inserted
      return false;
    }
  }
  public function editCompany($name, $owner, $loc, $id){
    $stmt = $this->conn->prepare("UPDATE companies SET name=?,owner=?,location=?, updated_at=NOW() WHERE id=?");
    $stmt->bind_param("ssss", $name,$owner,$loc, $id);
    if($stmt->execute()) {
      // Data EDITED
      return true;
    } else {
      // Not edited
      return false;
    }
  }
  public function delCompany($id){
    $stmt = $this->conn->prepare("DELETE FROM companies WHERE id=?");
    $stmt->bind_param("s", $id);
    if($stmt->execute()) {
      // Data deleted
      return true;
    } else {
      // Not deleted
      return false;
    }
  }
  public function getCompaniesJson(){
    $companies = array();
    $stmt=$this->conn->query("SELECT * FROM companies");
    while ($row=$stmt->fetch_assoc()) {
      # code...
      // $companies[]=$row;
      $id=$row['id'];
      $num_car_stmt=$this->conn->query("SELECT COUNT(id) as cars FROM cars WHERE company_id=$id");
      $result = $num_car_stmt->fetch_assoc();
      // $nberofcar=$result['cars'];
      $companies[]=array('id'=>$row['id'],'name'=>$row['name'],'owner'=>$row['owner'],'location'=>$row['location'],'created_at'=>$row['created_at'],'updated_at'=>$row['updated_at'],'cars'=>$result['cars']);
    }
    return $companies;
  }
  public function isNameExists($name, $id){
    if($id==null){
      $stmt = $this->conn->prepare("SELECT * FROM companies WHERE name=?");
      $stmt->bind_param("s", $name);
    }
    else{
      $stmt = $this->conn->prepare("SELECT * FROM companies WHERE name=? AND NOT id=?");
      $stmt->bind_param("ss", $name,$id);
    }
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
      // name existed 
      return true;
    } else {
      // name not existed
      return false;
    }
  }
  public function isCarExists($plate,$id){
    if($id==null){
      $stmt = $this->conn->prepare("SELECT * FROM cars WHERE plaque=?");
      $stmt->bind_param("s", $plate);
    }
    else{
      $stmt = $this->conn->prepare("SELECT * FROM cars WHERE plaque=? AND NOT id=?");
      $stmt->bind_param("ss", $plate,$id);
    }
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows > 0){
      // car existed 
      return true;
    }else{
      // car not existed
      return false;
    }
  }
  public function isCarBooked($id){
    $stmt = $this->conn->prepare("SELECT * FROM cars WHERE id=? AND is_booked='yes'");
    $stmt->bind_param("s",$id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows > 0){
      // car booked 
      return true;
    }else{
      // car not booked
      return false;
    }
  }
  public function setCarBooked($car){
    $stmt = $this->conn->prepare("UPDATE cars SET is_booked='yes' WHERE id=?");
    $stmt->bind_param("s",$car);
    if($stmt->execute()) {
      return true;
    } else {
      return false;
    }
  }
  public function addBooked($car,$booker,$from,$to){
    $stmt = $this->conn->prepare("INSERT INTO bookings(user_id,car_id,book_from,book_to) VALUES (?,?,?,?)");
    $stmt->bind_param("ssss",$booker,$car,$from,$to);
    if($stmt->execute()) {
      // Data inserted
      $this->setCarBooked($car);
      return true;
    } else {
      // Not inserted
      return false;
    }
  }
  public function bookChangeConfirm($id){
    $book = $this->findBookingById($id);
    $book_confirm;
    if($book==null) return false;
    else{
      if($book['confirm']=="no") $book_confirm="yes";
      else $book_confirm="no";
    }
    $stmt = $this->conn->prepare("UPDATE bookings SET confirm=?,updated_at=NOW() WHERE id=?");
    $stmt->bind_param("ss",$book_confirm,$id);
    if($stmt->execute()) {
      // Data deleted
      return true;
    } else {
      // Not deleted
      return false;
    }
  }
  public function addNotification($sender,$receiver,$content,$type){
    if($type===true){
      $stmt = $this->conn->prepare("INSERT INTO notifications(content, user_id_from,user_id_dest,is_for_company) VALUES (?,?,?,'yes')");
      $stmt->bind_param("sss",$content,$sender,$receiver);
    }else{
      $stmt = $this->conn->prepare("INSERT INTO notifications(content, user_id_from,user_id_dest) VALUES (?,?,?)");
      $stmt->bind_param("sss",$content,$sender,$receiver);
    }
    if($stmt->execute()) {
      return true;
    } else {
      return false;
    }
  }
  public function getAllBookings(){
    $list = array();
    $stmt=$this->conn->query("SELECT * FROM bookings");
    while ($row=$stmt->fetch_assoc()) {
      # code...
      $carid=$row['car_id'];
      $userid=$row['user_id'];
      $userinfo=$this->findUserById($userid);
      $carinfo=$this->findCarById($carid);
      $list[]=array(
        'id'=>$row['id'],
        'usernames'=>$userinfo['name'],
        'carname'=>$carinfo['name'],
        'from'=>$row['book_from'],
        'to'=>$row['book_to'],
        'confirm'=>$row['confirm'],
        'created_at'=>$row['created_at'],
        'updated_at'=>$row['updated_at']);
    }
    return $list;
  }
  public function getUserBookings($userid){
    $list = array();
    $stmt=$this->conn->query("SELECT * FROM bookings WHERE user_id=$userid");
    while ($row=$stmt->fetch_assoc()) {
      # code...
      $carid=$row['car_id'];
      $carinfo=$this->findCarById($carid);
      $list[]=array(
        'id'=>$row['id'],
        'carname'=>$carinfo['name'],
        'from'=>$row['book_from'],
        'to'=>$row['book_to'],
        'confirm'=>$row['confirm'],
        'created_at'=>$row['created_at'],
        'updated_at'=>$row['updated_at']);
    }
    return $list;
  }
  public function getNotifs($user, $type){
    $notifs=array();
    if($type=="user") {
      $query="SELECT * FROM notifications WHERE user_id_dest = $user ORDER BY created_at DESC";
    }
    else if($type=="owner"){
      $company_details=$this->findCompanyByOwner($user);
      $company_id=$company_details['id'];
      $query="SELECT * FROM notifications WHERE user_id_dest = $company_id ORDER BY created_at DESC";
    }
    else if($type=="all"){
      $query="SELECT * FROM notifications ORDER BY created_at DESC";
    }
    $stmt=$this->conn->query($query);
    while($row=$stmt->fetch_assoc()){
      $sender="";$receiver="";
      if($row['is_for_company']=="yes"){
        $comp=$this->findCompanyById($row['user_id_dest']);
        $receiver=$comp['name'];
      }else{
        $user=$this->findUserById($row['user_id_dest']);
        $receiver=$user['name'];
      }
      $user=$this->findUserById($row['user_id_from']);
      // $receiver=$user['name'];
      $sender=$user['name'];
      $notifs[]=array('id'=>$row['id'],'content'=>$row['content'],'user_id_from'=>$sender,'user_id_dest'=>$receiver,'is_for_company'=>$row['is_for_company'],'seen'=>$row['seen'],'created_at'=>$row['created_at'],'updated_at'=>$row['updated_at']);
    }
    return $notifs;
  }
  public function getDashBookings(){
    $bookings = array();
    $stmt=$this->conn->query("SELECT * FROM bookings");
    while ($row=$stmt->fetch_assoc()) {
      $user=$this->findUserById($row['user_id']);
      $car=$this->findCarById($row['car_id']);
      $bookings[]=array('id'=>$row['id'],'booker'=>$user['name'],'car'=>$car['name'],'created_at'=>$row['created_at']);
    }
    return $bookings;
  }
  public function searchForItem($id, $array){
    foreach ($array as $key => $val){
      if($val['id'] === $id){
        return $key;
      }
      return false;
    }
  }
}
?>