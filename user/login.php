<?php
try {
   session_start();
} catch(ErrorExpression $e) {
   session_regenerate_id();
   session_start();
} 
if(isset($_SESSION['user_id'])) header("Location:../home.php");
?>
<!DOCTYPE html>
<html ng-app="carRentingApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sign in @ CRS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" ng-controller="signInCtrl">
<div class="login-box">
  <div class="login-logo">
    <a href="../"><b>C</b>ar <b>R</b>enting <b>S</b>ystem</a>
  </div>
  <!-- /.login-logo -->
  <div class="btn btn-info" ng-show="isMessage" ng-bind="message"></div>
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <div class="form-group has-feedback">
      <input type="text" ng-model="user.email_uname" class="form-control" placeholder="Email or username">
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input type="password" ng-model="user.password" class="form-control" placeholder="Password">
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
      <!-- /.col -->
      <div class="col-xs-4">
        <button ng-click="login()" class="btn btn-primary btn-block btn-flat">Sign In</button>
      </div>
      <!-- /.col -->
    </div>
    <a href="register.php" class="text-center">Register a new membership</a>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script src="../dist/js/angular.1.6.9.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  var app = angular.module('carRentingApp', []);
  app.controller('signInCtrl', ($scope, $http, $window)=>{
    $scope.login=()=>{
      if(!$scope.user.email_uname||!$scope.user.password){
        $scope.isMessage=true;
        return $scope.message="Please fill the blank"
      }
      console.log('Data sent:'+JSON.stringify($scope.user))
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/login.php',
        data    : $scope.user, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        console.log('---'+JSON.stringify(res))
        if(res.data.error){
          $scope.isMessage=true;
          $scope.message=res.data.error_msg;
        }
        else $window.location.href="../home.php"
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.user={};
    $scope.isMessage=false;
  });
</script>
</body>
</html>
