<?php
try {
   session_start();
} catch(ErrorExpression $e) {
   session_regenerate_id();
   session_start();
} 
if(isset($_SESSION['user_id'])) header("Location:../home.php");
?>
<!DOCTYPE html>
<html ng-app="carRentingApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sign up @ CRS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page" ng-controller="signUpCtrl">
<div class="register-box">
  <div class="register-logo">
    <a href="../"><b>C</b>ar <b>R</b>enting <b>S</b>ystem</a>
  </div>
  <div class="btn btn-info" ng-show="isMessage" ng-bind="message"></div>
  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" ng-model="user.name" placeholder="Full name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" ng-model="user.username" placeholder="Enter user name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" ng-model="user.email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" ng-model="user.location" placeholder="Enter your location">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" ng-model="user.password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" ng-model="user.password2" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button ng-click="save()" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
      </div>
    <a href="login.php" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script src="../dist/js/angular.1.6.9.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  var app = angular.module('carRentingApp', []);
  app.controller('signUpCtrl', ($scope, $http, $window)=>{
    $scope.save=()=>{
      if(!$scope.user.name||!$scope.user.username||!$scope.user.email||!$scope.user.location||!$scope.user.password){
        $scope.isMessage=true;
        return $scope.message="Please fill the blank"
      }
      else if($scope.user.password!==$scope.user.password2){
        $scope.isMessage=true;
        return $scope.message="Password does not much"
      }
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/register.php',
        data    : $scope.user, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        console.log('----'+JSON.stringify(res))
        if(res.data.error){
          $scope.isMessage=true;
          $scope.message=res.data.error_msg;
          setTimeout(()=>{
            $scope.isMessage=false;
          },3000)
        }
        else $window.location.href="login.php";
      }).catch((err)=>{
        
      });
    }
    $scope.user={};
    $scope.isMessage=false;
  });
</script>
</body>
</html>
