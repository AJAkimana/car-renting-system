<div id="add_company_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
        <h4 class="modal-title">Register new company</h4>
        <h4 class="pull-right btn-danger" ng-show="isError" ng-bind="message"></h4>
        <h4 class="pull-right btn-success" ng-show="isSuccess" ng-bind="message"></h4>
      </div>  
      <div class="modal-body">
        <label>Company name</label>  
        <input type="text" class="form-control" ng-model="company.name" required />  
        <br /> 
        <label>Company owner</label>  
        <select class="form-control" ng-model="company.owner">
          <option value=""></option>
          <option ng-repeat="user in users" value="{{user.id}}">{{user.name}}</option>  
        </select> 
        <br />
        <label>Location</label>  
        <input type="text" class="form-control" ng-model="company.location" required />  
        <br>  
        <br>
        <button class="btn btn-primary" ng-click="addCompany()">Save data</button> 
        <button class="btn btn-default pull-right" data-dismiss="modal">Close</button> 
      </div>   
    </div>  
  </div>  
</div>
<div id="edit_company_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
        <h4 class="modal-title">Edit <b>{{currCompany.name|uppercase}}</b></h4>
      </div>  
      <div class="modal-body"> 
        <label>Company name</label>  
        <input type="text" class="form-control" ng-model="currCompany.name" required />  
        <br /> 
        <label>Company owner</label>  
        <select class="form-control" ng-model="currCompany.owner">
          <option value=""></option>
          <option ng-repeat="user in users" value="{{user.id}}">{{user.name}}</option>  
        </select> 
        <br />
        <label>Location</label>  
        <input type="text" class="form-control" ng-model="currCompany.location" required />  
        <br>  
        <br>
        <button class="btn btn-primary" ng-click="editCompany()">Save info</button> 
        <button class="btn btn-default pull-right" data-dismiss="modal">Close</button>  
      </div>   
    </div>  
  </div>  
</div>
<div id="delete_company_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
        <h4 class="modal-title">Delete <b>{{currCompany.name|uppercase}}</b></h4>
      </div>  
      <div class="modal-body">
        <h4 class="text-danger">Are you sure you want to delete this company <span class="glyphicon">&#xe129;</span> ?</h4> 
        <br>  
        <br>
        <button class="btn btn-danger" ng-click="delCompany()">Yes. Delete {{currCompany.name|uppercase}}</button> 
        <button class="btn btn-default pull-right" data-dismiss="modal">Close</button> 
      </div>   
    </div>  
  </div>  
</div>
<div id="add_car_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
        <h4 class="modal-title">Register new car</h4>
        <h4 class="pull-right btn-danger" ng-show="isError" ng-bind="message"></h4>
        <h4 class="pull-right btn-success" ng-show="isSuccess" ng-bind="message"></h4>
      </div>  
      <div class="modal-body">
        <label>Car name</label>  
        <input type="text" class="form-control" ng-model="car.name" required />
        <label>Car color</label>  
        <select class="form-control" ng-model="car.color">
          <option value=""></option>
          <option ng-repeat="color in colors" value="{{color}}">{{color|uppercase}}</option>  
        </select>
        <label>Car ID/Plaque</label>  
        <input type="text" class="form-control" ng-model="car.plate" required /> 
        <label>Type (Little or Big)</label>  
        <select class="form-control" ng-model="car.type">
          <option value=""></option>
          <option ng-repeat="type in types" value="{{type}}">{{type|uppercase}}</option>  
        </select>
        <?php if($user_access==1){ ?> 
          <label>Company owns CAR</label> 
          <select class="form-control" ng-model="car.company">
            <option value=""></option>
            <option ng-repeat="comp in companies" value="{{comp.id}}">{{comp.name|uppercase}}</option>  
          </select> 
        <?php } ?>
        <label>Booking price</label>  
        <input type="text" class="form-control" ng-model="car.price" required /> 
        <label>Car simple description</label> 
        <textarea ng-model="car.desc" class="form-control" placeholder="Enter description"></textarea>
        <br>  
        <br>
        <button class="btn btn-primary" ng-click="addCar()">Save data</button> 
        <button class="btn btn-default pull-right" data-dismiss="modal">Close</button> 
      </div>   
    </div>  
  </div>  
</div>
<div id="edit_car_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
        <h4 class="modal-title">Edit <b>{{currCar.name|uppercase}}</b></h4>
      </div>  
      <div class="modal-body">
        <label>Car name</label>  
        <input type="text" class="form-control" ng-model="currCar.name" required />
        <label>Car color</label>  
        <select class="form-control" ng-model="currCar.color">
          <option value=""></option>
          <option ng-repeat="color in colors" value="{{color}}">{{color|uppercase}}</option>  
        </select>
        <label>Car ID/Plaque</label>  
        <input type="text" class="form-control" ng-model="currCar.plaque" required /> 
        <label>Type (Little or Big)</label>  
        <select class="form-control" ng-model="currCar.type">
          <option value=""></option>
          <option ng-repeat="type in types" value="{{type}}">{{type|uppercase}}</option>  
        </select>
        <?php if($user_access==1){ ?> 
          <label>Company owns CAR</label>  
          <select class="form-control" ng-model="currCar.company_id">
            <option value=""></option>
            <option ng-repeat="comp in companies" value="{{comp.id}}">{{comp.name|uppercase}}</option>  
          </select> 
        <?php } ?>
        <label>Booking price</label>  
        <input type="text" class="form-control" ng-model="currCar.price" required /> 
        <label>Car simple description</label> 
        <textarea ng-model="currCar.description" class="form-control" placeholder="Enter description">{{currCar.description}}</textarea>
        <br>  
        <br>
        <button class="btn btn-primary" ng-click="editCar()">Save updated info</button> 
        <button class="btn btn-default pull-right" data-dismiss="modal">Close</button> 
      </div>   
    </div>  
  </div>  
</div>
<div id="delete_car_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <button type="button" class="close" data-dismiss="modal">&times;</button>  
        <h4 class="modal-title">Delete <b>{{currCar.name|uppercase}}</b></h4>
      </div>  
      <div class="modal-body">
        <h4 class="text-danger">Are you sure you want to delete this Car <span class="glyphicon">&#xe129;</span> ?</h4> 
        <h5 class="text-info text-center">If this car is booked will not be deleted except you cancel booking process</h5>
        <br>  
        <br>
        <button class="btn btn-danger" ng-click="delCar()">Yes. Delete {{currCar.name|uppercase}}</button> 
        <button class="btn btn-default pull-right" data-dismiss="modal">Close</button> 
      </div>   
    </div>  
  </div>  
</div>
<div id="add_car_image_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add image to the cars</h4>
      </div>  
      <div class="modal-body">
        <form method="post" action="../apis/insert_image.php" enctype="multipart/form-data">
          <label>Select car</label> 
          <?php if($user_access==1){ ?> 
            <select class="form-control" name="car_id">
              <option value=""></option>
              <option ng-repeat="car in cars" value="{{car.id}}">{{car.name|uppercase}}</option>  
            </select>
          <?php }else if($user_access==2){ ?>
            <select class="form-control" name="car_id">
              <option value=""></option>
              <option ng-repeat="car in comp_cars" value="{{car.id}}">{{car.name|uppercase}}</option>  
            </select>
          <?php } ?>
          <label>Car Image</label>  
          <input type="file" name="file_img" id="file_img" required/>
          <br>  
          <br>
          <input type="submit" name="save_image" value="Save image" class="btn btn-primary"/> 
          <button class="btn btn-default pull-right" data-dismiss="modal">Close</button> 
        </form>
      </div>   
    </div>  
  </div>  
</div>
<div id="login_register_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <h4 class="modal-title">Sorry, Be authenticated first</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>  
      <div class="modal-body">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#login">Log in</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#register">Sign up</a>
          </li>
        </ul>
        <div class="tab-content">
          <div id="login" class="container tab-pane active">
            <div class="form-group has-feedback">
              <input type="text" ng-model="log.email_uname" class="form-control" placeholder="Email or username">
            </div>
            <div class="form-group has-feedback">
              <input type="password" ng-model="log.password" class="form-control" placeholder="Password">
            </div>
            <div class="row">
              <div class="col-xs-4">
                <button ng-click="login()" class="btn btn-primary btn-block btn-flat">Sign In</button>
              </div>
            </div>
          </div>
          <div id="register" class="container tab-pane fade">
            <div class="form-group has-feedback">
              <input type="text" class="form-control" ng-model="user.name" placeholder="Full name">
            </div>
            <div class="form-group has-feedback">
              <input type="text" class="form-control" ng-model="user.username" placeholder="Enter user name">
            </div>
            <div class="form-group has-feedback">
              <input type="email" class="form-control" ng-model="user.email" placeholder="Email">
            </div>
            <div class="form-group has-feedback">
              <input type="text" class="form-control" ng-model="user.location" placeholder="Enter your location">
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" ng-model="user.password" placeholder="Password">
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" ng-model="user.password2" placeholder="Retype password">
            </div>
            <div class="row">
              <div class="col-xs-4">
                <button ng-click="register()" class="btn btn-primary btn-block btn-flat">Register</button>
              </div>
            </div>
          </div>
        </div>
      </div>   
    </div>  
  </div>  
</div>
  
<div id="book_Modal" class="modal fade">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">  
        <h4 class="modal-title">Book {{currCar.name|uppercase}} at {{currCar.price}} RwF</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>  
      <div class="modal-body">
        <div class="thumbnail">
          <a href="images/car_photos/{{currCar.images[0].image}}" target="_blank">
            <img src="images/car_photos/{{currCar.images[0].image}}" alt="Lights" style="width:100%">
            <div class="caption">
              <p ng-bind="currCar.description"></p>
            </div>
          </a>
        </div>
        <div class="dates" data-type="none">
          <label for="checkin">Check in</label>
          <div class="input-text">
            <input type="date" ng-model="currCar.checkin" placeholder="Select date" class="form-control"/>
          </div>
          <label for="checkout">Check out</label>
          <div class="input-text">
            <input type="date" ng-model="currCar.checkout" placeholder="Select date" class="form-control"/>
          </div>
        </div>
        <button class="btn btn-primary" ng-click="addBook()">Save info</button>
      </div>
    </div>
  </div>
</div>