<!--navbar -->
<div class="row">
  <div class="col-sm-12" class="navbar navbar-inverse navbar-fixed" >
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark " style="margin-top:10px;">
      <!-- Brand/logo -->
      <a class="navbar-brand" href="index.php">CAR BOOKING APP</a>

      <!-- Links -->
      <!-- <ul class="navbar-nav" style="margin-left:0px">
        <li class="dropdown"><a data-toggle="dropdown" class="nav-link"><strong>My card</strong></a>
        <ul class="dropdown-menu">
          <li>Change your settings</li>
          <li>New account</li>
          <li>Login</li>
          <li>Logout</li>
        </ul>
      </li> -->
        <li>
          <div class="btn-group">
            <button class="btn btn-simple">My card <span class="badge badge-success" ng-bind="cards.length"></span>
            </button>
            <button class="btn btn-simple dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
            </button>
            <div class="dropdown-menu">
              <label class="dropdown-item" ng-repeat="item in cards" ng-bind="item.name+' - '+item.price+'Rwf'">
              </label>
            </div>
          </div>
        </li>
        <?php
        if(!isset($_SESSION['user_id'])){
        ?>
          <li class="nav-item">
              <a class="nav-link" href="user/login.php">Sign in</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="user/register.php">Register</a>
          </li>
        <?php }else{ ?>
          <li class="nav-item">
            <a class="nav-link" href="home.php"><?php echo $_SESSION['user_names'] ?></a>
          </li>
        <?php } ?>
      </ul>
      <form style="margin-left:20px" class="form-inline">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Search</span>
          </div>
          <input type="text" class="form-control" placeholder="Car....">
        </div>
      </form>
    </nav>
  </div>
</div>
<!--navbar -->