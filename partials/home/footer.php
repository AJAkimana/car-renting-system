<!--start footer-->
<div class="row">
  <div class="col-sm-12" >
    <footer id="myFooter">
      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <h5 style="color:#411C1C">ABOUT US</h5>
            <ul>
              <li><a href="#">ABOUT US</a></li>
              <li><a href="#">TERMS OF USE</a></li>
              <li><a href="#">FAQs</a></li>
              <li><a href="#">PRIVACY</a></li>
              <li><a href="#">ADMIN</a></li>
            </ul>
          </div>
          <div class="col-sm-3">
            <h5 style="color:#411C1C">SERVICES</h5>
            <ul>
              <li><a href="#">RENT</a></li>
              <li><a href="#">BOOKINGS</a></li>
              <li><a href="#">DRIVERS</a></li>
              <li><a href="#">CARS</a></li>
            </ul>
          </div>
          <div class="col-sm-3">
            <h5 style="text-align:center; color:#411C1C">OUR SOCIAL MEDIA</h5>
            <div class="social-networks">
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="whatsapp"><i class="fa fa-whatsapp"></i></a>
              <a href="#" class="youtube"><i class="fa fa-youtube"></i></a>
            </div>
          </div>

          <div class="col-sm-3">
            <h5 style="text-align:center; color:#411C1C">SUBSCRIBE TO OUR NEWSLETTER</h5>
            <div class="form-group">
              <form class="form-inline" action="#">
                <input class="form-control mr-sm-1" type="email" placeholder="E-mail">
                <button class="btn btn-success" type="submit">Subscribe</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <p>© 2018 Copyright Nide </p>
      </div>
    </footer>
  </div>
</div>
<script type="text/javascript">
  var app = angular.module('carRentingApp', []);
  app.controller('homeCtrl', ($scope, $http, $window)=>{
    $scope.openModal=(car)=>{
      $scope.currCar=car;
      if($scope.user==""){
        $('#login_register_Modal').modal('show');
      }else{
        $('#book_Modal').modal('show');
      }
      $scope.currCar.checkin=new Date();
      $scope.currCar.checkout=new Date();
    }
    $scope.addBook=()=>{
      $scope.currCar.booker=$scope.user;
      $http({
        method:'POST',
        url:'/carrenting/apis/controllers.php?action=book',
        data:$scope.currCar, //forms user object
        headers:{'Content-Type': 'application/x-www-form-urlencoded'}
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          $scope.getCars();
          $('#book_Modal').modal('hide');
          Notifier.success($scope.currCar.name.toUpperCase()+" successfully booked. Wait for owner")
        }
      }).catch((err)=>{
        Notifier.danger('Error:'+err)
      });
    }
    $scope.getCars=()=>{
      $http.get('apis/controllers.php?cars_list')
      .then((res)=>{
        var data = res.data;
        if(data.error){
          $scope.isMessage=true;
          $scope.message=data.error_msg;
        }
        else{
          $scope.cars=data.cars;
        }
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.getCardsList=()=>{
      $http.get('apis/controllers.php?action=view_all')
      .then((res)=>{
        var data = res.data;
        if(data.error){
          Notifier(data.error_msg);
        }
        else{
          $scope.cards=data.cards;
        }
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.addToCard=(car)=>{
      $http({
        method:'POST',
        url:'/carrenting/apis/controllers.php?action=add',
        data:car, //forms user object
        headers:{'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          $scope.getCardsList();
          Notifier.success(car.name.toUpperCase()+" added")
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.rmvToCard=(car)=>{
      $http({
        method:'POST',
        url:'/carrenting/apis/controllers.php?action=remove',
        data:car, //forms user object
        headers:{'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          $scope.getCardsList();
          Notifier.success(car.name.toUpperCase()+" removed")
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.login=()=>{
      if(!$scope.log) return Notifier.danger("Please fill the blank")
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/login.php',
        data    : $scope.log, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(data.error_msg);
        else{
          $('#login_register_Modal').modal('hide');
          $('#book_Modal').modal('show');
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.register=()=>{
      if(!$scope.user) return Notifier.danger("Please fill the blank")
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/register.php',
        data    : $scope.user, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(data.error_msg);
        else{
          $('#login_register_Modal').modal('hide');
          $('#book_Modal').modal('show');
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.getCars();
    $scope.getCardsList();
    $scope.user=$('#user_id').val();
  })
</script>
<!--end footer -->