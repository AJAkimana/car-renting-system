<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<link rel="stylesheet" href="dist/css/Footer-with-button-logo.css">
	<link rel="stylesheet" href="dist/bootstrap-4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.min.css">
	<script src="dist/bootstrap-4.0.0/js/jquery-3.2.1.min.js"></script>
	<script src="dist/bootstrap-4.0.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="dist/js/bs_notify.js"></script>
	<script src="dist/js/angular.1.6.9.min.js"></script>
	<script src="dist/js/MIT_sweetalert2.js"></script>
	<script src="dist/js/lionel_script.js"></script>
</head>