<?php
  include '../partials/header_menu.php';
 ?>
  <div class="content-wrapper" ng-controller="notifsCtrl">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $userType ?> panel</small>
      </h1>
      <?php if($user_access==1){ ?>
        <ol class="breadcrumb">
          <li><a href="."><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Notifications</li>
        </ol>
      <?php }else if($user_access==2){ ?>
        <ol class="breadcrumb">
          <li><a href="cars.php"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Notifications</li>
        </ol>
      <?php }else{ ?>
        <ol class="breadcrumb">
          <li><a href="booking.php"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Notifications</li>
        </ol>
      <?php } ?>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-lg-6">
          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h1 class="box-title">Notifications</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class='col-md-9' ng-repeat="notif in notifs">
                <p style=' border-radius:12px; padding: 10px 10px 10px 10px;background-color: #3c8dbc; color:white;'>
                  <img src="" height="4%" width="5%"><b>{{notif.user_id_dest}}</b>
                  <br/> 
                  <i class='glyphicon glyphicon-share-alt' ng-bind="notif.created_at"></i>
                  <br/><br/>
                  {{notif.content}}
                  <br/>
                </p>
             </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- Model includes -->
    <?php include '../partials/modals.php'; ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include '../partials/footer.php'; ?>
<script>
  var app = angular.module('carRentingApp', []);
  app.controller('notifsCtrl', ($scope, $http, $window)=>{
    $scope.getUserNotifications=()=>{
      $http.get('../apis/controllers.php?user_notifs')
      .then((res)=>{
        var data = res.data;
        if(data.error) Notifier.danger(data.error_msg);
        else{
          $scope.notifs=data.notifs;
        }
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.getUserNotifications()
  })
</script>
</body>
</html>