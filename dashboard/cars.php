<?php
  include '../partials/header_menu.php';
?>
  <!--  Header Header Header Header Header Header Header Header Header Header -->
  <!-- Left side column. contains the logo and sidebar -->
  <!-- Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" ng-controller="carCtrl">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $userType ?> panel</small>
      </h1>
      <?php if($user_access==1){ ?>
        <ol class="breadcrumb">
          <li><a href="."><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Cars</li>
        </ol>
      <?php }else if($user_access==2){ ?>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
      <?php } ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="box-footer clearfix no-border pull-right">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_car_image_Modal"><i class="fa fa-plus"></i>Add car image</button>
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_car_Modal"><i class="fa fa-plus"></i> Register car</button>
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-lg-12">
          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title">List of registed cars</h3>
              <?php if($user_access==2){ ?>
              <h3 class="box-title pull-right" ng-bind="company_name"></h3>
              <?php } ?>
            </div>
            <!-- /.box-header -->
            <?php if($user_access==1){ ?>
              <div class="box-body table-responsive">
                <!-- Users data -->
                <table class="table" ng-show="cars.length">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Color</th>
                      <th>No of images</th>
                      <th>Plaque</th>
                      <th>Type</th>
                      <th>Booked</th>
                      <th>Booking price</th>
                      <th>Active</th>
                      <th>Created</th>
                      <th>Updated</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="car in cars">
                      <td ng-bind="$index+1"></td>
                      <td>
                        <a href="view_car.php?car={{car.id}}" ng-bind="car.name">
                        </a>
                      </td>
                      <td ng-bind="car.color"></td>
                      <td ng-bind="car.images"></td>
                      <td ng-bind="car.plaque"></td>
                      <td ng-bind="car.type"></td>
                      <td ng-bind="car.is_booked"></td>
                      <td ng-bind="car.price"></td>
                      <td>
                        <button class="btn btn-simple" ng-click="setActive(car.id,car.name)" ng-bind="car.is_active"></button>  
                      </td>
                      <td ng-bind="car.created_at"></td>
                      <td ng-bind="car.updated_at"></td>
                      <td>
                        <button class="btn btn-info" data-toggle="modal" data-target="#edit_car_Modal" ng-click="setCurrent(car)">Edit</button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#delete_car_Modal" ng-click="setCurrent(car)">Delete</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <h4 ng-show="!cars.length">No car registered</h4>
              </div>
            <?php }else if($user_access==2){ ?>
              <div class="box-body table-responsive">
                <!-- Users data -->
                <table class="table" ng-show="comp_cars.length">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Color</th>
                      <th>No of images</th>
                      <th>Plaque</th>
                      <th>Type</th>
                      <th>Booked</th>
                      <th>Booking price</th>
                      <th>Active</th>
                      <th>Created</th>
                      <th>Updated</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="car in comp_cars">
                      <td ng-bind="$index+1"></td>
                      <td>
                        <a href="view_car.php?car={{car.id}}" ng-bind="car.name">
                        </a>
                      </td>
                      <td ng-bind="car.color"></td>
                      <td ng-bind="car.images"></td>
                      <td ng-bind="car.plaque"></td>
                      <td ng-bind="car.type"></td>
                      <td ng-bind="car.is_booked"></td>
                      <td ng-bind="car.price"></td>
                      <td>
                        <button class="btn btn-simple" ng-click="setActive(car.id,car.name)" ng-bind="car.is_active"></button>  
                      </td>
                      <td ng-bind="car.created_at"></td>
                      <td ng-bind="car.updated_at"></td>
                      <td>
                        <button class="btn btn-info" data-toggle="modal" data-target="#edit_car_Modal" ng-click="setCurrent(car)">Edit</button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#delete_car_Modal" ng-click="setCurrent(car)">Delete</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <h4 ng-show="!comp_cars.length">Your company has no car registered</h4>
              </div>
            <?php } ?>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- Model includes -->
    <?php include '../partials/modals.php'; ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include '../partials/footer.php'; ?>
<script>
  var app = angular.module('carRentingApp', []);
  app.controller('carCtrl', ($scope, $http, $window)=>{
    $scope.addCar=()=>{
      if(!$scope.car){
        return Notifier.danger("Specify car information");
      }
      $http({
        method:'POST',
        url:'/carrenting/apis/controllers.php?add_car',
        data:$scope.car, //forms user object
        headers:{'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success("Successfully registered")
          $scope.getCars();
          $scope.getCompanyCars();
          $('#add_car_Modal').modal('hide');
          $scope.car=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.getCars=()=>{
      $http.get('../apis/controllers.php?car')
      .then((res)=>{
        var data = res.data;
        if(data.error){
          $scope.isMessage=true;
          $scope.message=data.error_msg;
        }
        else{
          $scope.cars=data.cars;
        }
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.getCompanyCars=()=>{
      $http.get('../apis/controllers.php?company_car')
      .then((res)=>{
        var data = res.data;
        if(data.error){
          $scope.isMessage=true;
          $scope.message=data.error_msg;
        }
        else{
          $scope.comp_cars=data.cars;
          $scope.company_name=data.company;
        }
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.setCurrent=(car)=>{
      $scope.currCar=car;
    }
    $scope.editCar=()=>{
      if(!$scope.currCar){
        return Notifier.danger("Please enter Car information");
      }
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/controllers.php?edit_car',
        data    : $scope.currCar, //forms object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success($scope.currCar.name.toUpperCase()+" has edited")
          $scope.getCars();
          $scope.getCompanyCars();
          $('#edit_car_Modal').modal('hide');
          $scope.currCar=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.setActive=(car_id, name)=>{
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/controllers.php?set_active_car',
        data    : {car_id:car_id}, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success(name.toUpperCase()+" has changed")
          $scope.getCars();
          $scope.getCompanyCars();
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.delCar=()=>{
      if(!$scope.currCar){
        return Notifier.danger("Please enter company information");
      }
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/controllers.php?del_car',
        data    : $scope.currCar, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success($scope.currCar.name.toUpperCase()+" has deleted")
          $scope.getCars();
          $scope.getCompanyCars();
          $('#delete_car_Modal').modal('hide');
          $scope.currCar=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.getCompanies=()=>{
      $http.get('../apis/controllers.php?company')
      .then((res)=>{
        var data = res.data;
        if(data.error) Notifier(data.error_msg);
        else $scope.companies=res.data.companies;
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.colors=['red','green','blue','white','brown'];
    $scope.types=['little','big'];
    $scope.getCompanies();
    $scope.getCars();
    $scope.getCompanyCars();
  })
</script>
</body>
</html>