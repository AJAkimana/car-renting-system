<?php
  include '../partials/header_menu.php';
 ?>
  <!--  Header Header Header Header Header Header Header Header Header Header -->
  <!-- Left side column. contains the logo and sidebar -->
  <!-- Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" ng-controller="bookingCtrl">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $userType ?> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Bookings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">  
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-lg-12">
          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title">List of all booking requests</h3>
              <a href="../" class="btn btn-simple btn-primary pull-right">Add booking</a>
            </div>
            <!-- /.box-header -->
            <?php if($user_access==1||$user_access==2||$user_access==3){
            ?>
              <div class="box-body table-responsive">
                <!-- Companies data -->
                <table class="table" ng-show="bookings">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>User</th>
                      <th>Car</th>
                      <th>Check in</th>
                      <th>Check out</th>
                      <th>Is confirmed</th>
                      <th>Created</th>
                      <th>Updated</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="book in bookings">
                      <td ng-bind="$index+1"></td>
                      <td ng-bind="book.usernames"></td>
                      <td ng-bind="book.carname"></td>
                      <td ng-bind="book.from"></td>
                      <td ng-bind="book.to"></td>
                      <td>
                        <button class="btn btn-simple" ng-click="setConfirm(book)" ng-bind="book.confirm"></button> 
                      </td>
                      <td ng-bind="book.created_at"></td>
                      <td ng-bind="book.updated_at"></td>
                      <td>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#delete_company_Modal" ng-click="setCurrent(comp)">Delete</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <h4 ng-show="!bookings">No request found</h4>
              </div>
            <?php }else{ ?>
              <div class="box-body table-responsive">
                <!-- Companies data -->
                <table class="table" ng-show="user_books">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Car</th>
                      <th>Check in</th>
                      <th>Check out</th>
                      <th>Is confirmed</th>
                      <th>Created</th>
                      <th>Updated</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="userb in user_books">
                      <td ng-bind="$index+1"></td>
                      <td ng-bind="userb.carname"></td>
                      <td ng-bind="userb.from"></td>
                      <td ng-bind="userb.to"></td>
                      <td ng-bind="userb.confirm"></td>
                      <td ng-bind="userb.created_at"></td>
                      <td ng-bind="userb.updated_at"></td>
                      <td>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#delete_company_Modal" ng-click="setCurrent(userb)">Delete</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <h4 ng-show="!user_books">You havent requested</h4>
              </div>
              <?php } ?>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- Model includes -->
    <?php include '../partials/modals.php'; ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include '../partials/footer.php'; ?>
<script>
  var app = angular.module('carRentingApp', []);
  app.controller('bookingCtrl', ($scope, $http, $window)=>{
    $scope.setCurrent=(book)=>{
      $scope.currBook=book;
    }
    $scope.setConfirm=(book)=>{
      book.user_id=$scope.user;
      $http({
        method: 'POST',
        url:'/carrenting/apis/controllers.php?confirm_book',
        data:book, //forms user object
        headers:{'Content-Type': 'application/x-www-form-urlencoded'}
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success("Confirm changed")
          $scope.getBookings();
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.getBookings=()=>{
      $http.get('../apis/controllers.php?bookings')
      .then((res)=>{
        var data = res.data;
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else $scope.bookings=data.bookings;
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.getUserBookings=()=>{
      $http.get('../apis/controllers.php?user_bookings')
      .then((res)=>{
        var data = res.data;
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else $scope.user_books=data.bookings;
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.user=$('#user_id').val();
    $scope.getBookings();
    $scope.getUserBookings();
  })
</script>
</body>
</html>