<?php
  include '../partials/header_menu.php';
 ?>
  <!--  Header Header Header Header Header Header Header Header Header Header -->
  <!-- Left side column. contains the logo and sidebar -->
  <!-- Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" ng-controller="dashCtrl">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $userType ?> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="box-footer clearfix no-border pull-right">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_car_Modal"><i class="fa fa-plus"></i> Register car</button>
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_company_Modal"><i class="fa fa-plus"></i> Add company</button>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 ng-bind="users_n"></h3>

              <p>Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
              <!-- <i class="ion ion-bag"></i> -->
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 ng-bind="companies_n"></h3>

              <p>Companies</p>
            </div>
            <div class="icon">
              <i class="ion ion-easel"></i>
            </div>
            <a href="company.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 ng-bind="cars_n"></h3>

              <p>Cars</p>
            </div>
            <div class="icon">
              <i class="ion ion-model-s"></i>
            </div>
            <a href="cars.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 ng-bind="bookings_n"></h3>
              <p>Bookings</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="booking.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-lg-8">
          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title">Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <!-- Users data -->
              <table class="table" ng-show="users.length">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>User name</th>
                    <th>Email</th>
                    <th>Access Level</th>
                    <th>Location</th>
                    <th>Active status</th>
                    <th>Created at</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="user in users">
                    <td ng-bind="$index+1"></td>
                    <td ng-bind="user.name"></td>
                    <td ng-bind="user.username"></td>
                    <td ng-bind="user.email"></td>
                    <td ng-bind="user.access_lv"></td>
                    <td ng-bind="user.location"></td>
                    <td ng-bind="user.is_active"></td>
                    <td ng-bind="user.created_at"></td>
                  </tr>
                </tbody>
              </table>
              <p ng-show="!users.length" class="text-info">No user found</p>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-lg-4">
          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Bookings</h3>

              <div class="box-tools pull-right">
                <ul class="pagination pagination-sm inline">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="todo-list" ng-show="all_books.length">
                <li ng-repeat="book in all_books">
                  <span class="handle">
                    <i class="fa fa-ellipsis-v"></i>
                    <i class="fa fa-ellipsis-v"></i>
                  </span>
                  <span class="text" ng-bind="book.booker+' --> '+book.car"></span>
                  <small class="label label-danger"><i class="fa fa-clock-o"></i> {{book.created_at}}</small>
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div>
                </li>
              </ul>
              <h4 ng-show="!all_books.length">No booking found</h4>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- Model includes -->
    <?php include '../partials/modals.php'; ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include '../partials/footer.php'; ?>
<script>
  var app = angular.module('carRentingApp', []);
  app.controller('dashCtrl', ($scope, $http, $window)=>{
    $scope.getDashBoard=()=>{
      $http.get('../apis/controllers.php?admin_dash')
      .then((res)=>{
        var data = res.data;
        if(data.error) Notifier.danger(data.error_msg);
        else{
          $scope.users_n=data.dashbord.users.number;
          $scope.cars_n=data.dashbord.cars;
          $scope.companies_n=data.dashbord.companies;
          $scope.bookings_n=data.dashbord.bookings.number;
          $scope.users=data.dashbord.users.list;
          $scope.bookings=data.dashbord.bookings.list;
          // console.log('Data:'+JSON.stringify(data))
        }
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.getDashBooks=()=>{
      $http.get('../apis/controllers.php?books_dash')
      .then((res)=>{
        var data = res.data;
        if(data.error) Notifier.danger(data.error_msg);
        else $scope.all_books=data.bookings;
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.getUsers=()=>{
      $http.get('../apis/controllers.php?users')
      .then((res)=>{
        var data = res.data;
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else $scope.users=data.users;
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.addCar=()=>{
      if(!$scope.car){
        return Notifier.danger("Specify car information");
      }
      $http({
        method:'POST',
        url:'/carrenting/apis/controllers.php?add_car',
        data:$scope.car, //forms user object
        headers:{'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success("Successfully registered");
          $scope.getDashBoard();
          $('#add_car_Modal').modal('hide');
          $scope.car=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.addCompany=()=>{
      if(!$scope.company){
        return Notifier.danger("Please enter company information");
      }
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/controllers.php?add_company',
        data    : $scope.company, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success("Successfully registered")
          $scope.getDashBoard();
          $('#add_company_Modal').modal('hide');
          $scope.company=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.getCompanies=()=>{
      $http.get('../apis/controllers.php?company')
      .then((res)=>{
        var data = res.data;
        if(data.error) Notifier(data.error_msg);
        else $scope.companies=res.data.companies;
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.colors=['red','green','blue','white','brown'];
    $scope.types=['little','big'];
    $scope.getUsers();
    $scope.getDashBoard();
    $scope.getDashBooks();
    $scope.getCompanies();
  })
</script>
</body>
</html>