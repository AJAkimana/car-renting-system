<?php
  include '../partials/header_menu.php';
 ?>
  <!--  Header Header Header Header Header Header Header Header Header Header -->
  <!-- Left side column. contains the logo and sidebar -->
  <!-- Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" ng-controller="companyCtrl">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $userType ?> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="."><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Companies</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="box-footer clearfix no-border pull-right">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_car_Modal"><i class="fa fa-plus"></i> Register car</button>
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_company_Modal"><i class="fa fa-plus"></i> Add company</button>
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-lg-12">
          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title">List of registed companies</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <!-- Companies data -->
              <table class="table" ng-show="companies.length">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Company name</th>
                    <th>Number of cars</th>
                    <th>Location</th>
                    <th>Company owner</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="comp in companies">
                    <td ng-bind="$index+1"></td>
                    <td ng-bind="comp.name"></td>
                    <td ng-bind="comp.cars"></td>
                    <td ng-bind="comp.location"></td>
                    <td ng-bind="comp.owner|toCompany:users"></td>
                    <td ng-bind="comp.created_at"></td>
                    <td ng-bind="comp.updated_at"></td>
                    <td>
                      <button class="btn btn-info" data-toggle="modal" data-target="#edit_company_Modal" ng-click="setCurrent(comp)">Edit</button>
                      <button class="btn btn-danger" data-toggle="modal" data-target="#delete_company_Modal" ng-click="setCurrent(comp)">Delete</button>
                    </td>
                  </tr>
                </tbody>
              </table>
              <h4 ng-show="!companies.length">No company registered</h4>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- Model includes -->
    <?php include '../partials/modals.php'; ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include '../partials/footer.php'; ?>
<script>
  var app = angular.module('carRentingApp', []);
  app.filter('toCompany', function(){
    return function(input,users){
      if(input==''||!input) return 'Not defined';
      for(var i =0;i < users.length;i++){
        if(users[i].id == input) return users[i].name.toUpperCase();
      }
    }
  }).controller('companyCtrl', ($scope, $http, $window)=>{
    $scope.addCar=()=>{
      if(!$scope.car){
        return Notifier.danger("Specify car information");
      }
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/controllers.php?add_car',
        data    : $scope.car, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success("Successfully registered")
          $scope.getCompanies();
          $('#add_car_Modal').modal('hide');
          $scope.car=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.addCompany=()=>{
      if(!$scope.company){
        return Notifier.danger("Please enter company information");
      }
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/controllers.php?add_company',
        data    : $scope.company, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success("Successfully registered")
          $scope.getCompanies();
          $('#add_company_Modal').modal('hide');
          $scope.company=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.editCompany=()=>{
      if(!$scope.currCompany){
        return Notifier.danger("Please enter company information");
      }
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/controllers.php?edit_company',
        data    : $scope.currCompany, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success($scope.currCompany.name.toUpperCase()+" has edited")
          $scope.getCompanies();
          $('#edit_company_Modal').modal('hide');
          $scope.currCompany=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.delCompany=()=>{
      if(!$scope.currCompany){
        return Notifier.danger("Please enter company information");
      }
      $http({
        method  : 'POST',
        url     : '/carrenting/apis/controllers.php?del_company',
        data    : $scope.currCompany, //forms user object
        headers : {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then((res)=>{
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else{
          Notifier.success($scope.currCompany.name.toUpperCase()+" has deleted")
          $scope.getCompanies();
          $('#delete_company_Modal').modal('hide');
          $scope.currCompany=null;
        }
      }).catch((err)=>{
        console.log('Error:'+err)
      });
    }
    $scope.setCurrent=(company)=>{
      $scope.currCompany=company;
    }
    $scope.getCompanies=()=>{
      $http.get('../apis/controllers.php?company')
      .then((res)=>{
        var data = res.data;
        if(data.error) Notifier(data.error_msg);
        else $scope.companies=res.data.companies;
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.getUsers=()=>{
      $http.get('../apis/controllers.php?users')
      .then((res)=>{
        var data = res.data;
        if(res.data.error) Notifier.danger(res.data.error_msg)
        else $scope.users=data.users;
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.colors=['red','green','blue','white','brown'];
    $scope.types=['little','big'];
    $scope.getUsers()
    $scope.getCompanies();
  })
</script>
</body>
</html>