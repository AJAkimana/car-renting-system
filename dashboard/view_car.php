<?php
  include '../partials/header_menu.php';
  if(!isset($_GET['car'])&&$_GET['car']){
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
  }
  $car_id=$_GET['car'];
  $stmt=$conn->query("SELECT * FROM cars WHERE id=$car_id");
  $result = $stmt->fetch_assoc();
 ?>
  <!--  Header Header Header Header Header Header Header Header Header Header -->
  <!-- Left side column. contains the logo and sidebar -->
  <!-- Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside Aside -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" ng-controller="viewCarCtrl">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $userType ?> panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="."><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="cars.php">Cars</a></li>
        <li class="active"><?php echo $result['name']; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="box-footer clearfix no-border pull-right">
          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_car_image_Modal"><i class="fa fa-plus"></i>Add car image</button>
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-lg-12">
          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h1 class="box-title"><?php echo $result['name']; ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-3" ng-repeat="img in images">
                <!-- <label>Hey it is working</label> -->
                <img src="../images/car_photos/{{img.image}}"  class="img-rounded" alt="Car image" width="50%"/>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- Model includes -->
    <?php include '../partials/modals.php'; ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include '../partials/footer.php'; ?>
<script>
  var app = angular.module('carRentingApp', []);
  app.controller('viewCarCtrl', ($scope, $http, $window)=>{
    $scope.getCarImages=()=>{
      $http.get('../apis/controllers.php?images&car_id=<?php echo $car_id ?>')
      .then((res)=>{
        var data = res.data;
        if(data.error){
          Notifier(data.error_msg);
        }
        else{
          $scope.images=data.images;
          // console.log(JSON.stringify($scope.images))
        }
      }).catch((err)=>{
        console.log('Error:')
      })
    }
    $scope.getCars=()=>{
      $http.get('../apis/controllers.php?car')
      .then((res)=>{
        var data = res.data;
        if(data.error){
          $scope.isMessage=true;
          $scope.message=data.error_msg;
        }
        else{
          $scope.cars=data.cars;
        }
      }).catch((err)=>{
        console.log('Error')
      })
    }
    $scope.getCarImages();
    $scope.getCars();
  })
</script>
</body>
</html>