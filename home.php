<?php 
session_start();
if(isset($_SESSION['user_id'])){
	if ($_SESSION['user_access']==4) {
		# code...
		header('Location:dashboard/booking.php');
	}else if ($_SESSION['user_access']==2) {
		# code...
		header('Location:dashboard/cars.php');
	}else if ($_SESSION['user_access']==1) {
		# code...
		header('Location:dashboard/index.php');
	}else{
		header('Location:.');
	}
}else{
	header('Location:.');
}
?>